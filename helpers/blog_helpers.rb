def featured_articles
  blog.articles.select {|article| article.data.key? :featured }
end
